### How do I get set up? ###

Ensure you have node, yarn, brew, watchman, cocoapods installed

To run the project, you will need Node, Watchman, React-Native CLI and Xcode or Android SDK depending on your chosen platform, you can install these using Homebrew

brew install node

brew install watchman

Node comes with npm, you can use Yarn if you prefer or any other package manager.

npm install -g react-native-cli

Android SDK or Xcode
Get Xcode from the Mac App Store


About the app 
It's a React Native Dashboard App with restaurant names listing and anotheer screen with it's relavant graphs
The listing screen has functionality such as Add, Edit and Delete 
